package com.jay.config;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.JSONToken;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.jay.entity.BaseEnum;
import com.jay.entity.DefaultBaseEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.ArrayUtil;

import java.lang.reflect.Type;

@Slf4j
public class EnumDeserializer implements ObjectDeserializer {
    /**
     * fastjson invokes this call-back method during deserialization when it encounters a field of the
     * specified type.
     * <p>In the implementation of this call-back method, you should consider invoking
     * {@link com.alibaba.fastjson.JSON#parseObject(String, Type, com.alibaba.fastjson.parser.Feature[])} method to create objects
     * for any non-trivial field of the returned object.
     *
     * @param parser    context DefaultJSONParser being deserialized
     * @param type      The type of the Object to deserialize to
     * @param fieldName parent object field name
     * @return a deserialized object of the specified type which is a subclass of {@code T}
     */
    @Override
    public <T> T deserialze(DefaultJSONParser parser, Type type, Object fieldName) {
        try {
            log.info(parser.getInput());
            DefaultBaseEnum defaultBaseEnum = parser.parseObject(DefaultBaseEnum.class, fieldName);
            Integer value = defaultBaseEnum.getValue();
            T enumConstant = getEnum(type, value);
            if (enumConstant != null) {
                return enumConstant;
            }
            return null;
        } catch (Exception e) {
            log.debug("", e);
        }

        final JSONLexer lexer = parser.lexer;
        final int token = lexer.token();
        Class cls = (Class) type;
        T enumConstant = getEnum(type, lexer.intValue());
        if (enumConstant != null) {
            return enumConstant;
        }
        //没实现EnumValue接口的 默认的按名字或者按ordinal
        if (token == JSONToken.LITERAL_INT) {
            int intValue = lexer.intValue();
            lexer.nextToken(JSONToken.COMMA);
            Object[] enumConstants = cls.getEnumConstants();
            if (intValue < 0 || intValue > enumConstants.length) {
                throw new JSONException("parse enum " + cls.getName() + " error, value : " + intValue);
            }
            return (T) enumConstants[intValue];
        }
        return (T) Enum.valueOf(cls, lexer.stringVal());
    }

    private <T> T getEnum(Type type, Integer value) {
        Class cls = (Class) type;
        Object[] enumConstants = cls.getEnumConstants();
        if (BaseEnum.class.isAssignableFrom(cls)) {
            for (Object enumConstant : enumConstants) {
                if (((BaseEnum) enumConstant).getValue().equals(value)) {
                    return (T) enumConstant;
                }
            }
        }
        return null;
    }

    @Override
    public int getFastMatchToken() {
        return 0;
    }
}
