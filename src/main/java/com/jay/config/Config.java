package com.jay.config;

import com.alibaba.fastjson.serializer.EnumerationSerializer;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ToStringSerializer;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.jay.entity.DataEnum;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * 项目配置类
 *
 * @author jay
 */
@Configuration
public class Config {

    @Bean
    public HttpMessageConverters fastJsonMessageConverters() {
        List<HttpMessageConverter<?>> converters = new ArrayList<>();
        //需要定义一个convert转换消息的对象;
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        //添加fastJson的配置信息;
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);

        //全局时间配置
        fastJsonConfig.setCharset(StandardCharsets.UTF_8);
        //处理中文乱码问题
        List<MediaType> fastMediaTypes = new ArrayList<>();
        fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
        //在convert中添加配置信息.
        fastConverter.setSupportedMediaTypes(fastMediaTypes);
        SerializeConfig serializeConfig = SerializeConfig.getGlobalInstance();
        serializeConfig.put(BigInteger.class, ToStringSerializer.instance);
        serializeConfig.put(Long.class, ToStringSerializer.instance);
        serializeConfig.put(Long.TYPE, ToStringSerializer.instance);
        fastJsonConfig.setSerializeConfig(serializeConfig);
        fastConverter.setFastJsonConfig(fastJsonConfig);
        converters.add(0, fastConverter);
        return new HttpMessageConverters(converters);
    }


//    @Bean
//    public FastJsonConfig fastjsonConfig(@Qualifier("") List<Class<?>> enumValues) {
//        FastJsonConfig config = new FastJsonConfig();
//        config.setSerializerFeatures(SerializerFeature.WriteDateUseDateFormat);
//
//        // TODO 这里只是为了测试, 最好都通过扫描来查找而不是硬编码
//        // enumValues.add(Sex.class);
//
//        if (enumValues != null && enumValues.size() > 0) {
//            // 枚举类型字段：序列化反序列化配置
//            EnumConverter enumConverter = new EnumConverter();
//            ParserConfig parserConfig = config.getParserConfig();
//            SerializeConfig serializeConfig = config.getSerializeConfig();
//            for (Class<?> clazz : enumValues) {
//                parserConfig.putDeserializer(clazz, enumConverter);
//                serializeConfig.put(clazz, enumConverter);
//            }
//        }
//
//        return config;
//    }
}
