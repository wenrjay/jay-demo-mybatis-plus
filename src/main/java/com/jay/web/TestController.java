package com.jay.web;

import com.jay.dto.JsonData;
import com.jay.entity.DataEnum;
import com.jay.entity.JsonDemoEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class TestController {

    @GetMapping("/test/get")
    public JsonDemoEntity get() {
        JsonDemoEntity entity = new JsonDemoEntity();
        entity.setId(6579200253588676608L);
        entity.setName("jar");
        entity.setJsonData(new JsonData(0L, "test"));
        entity.setDataType(DataEnum.KEY_OK);
        entity.setCreateTime(new Date());
        return entity;
    }

    @PostMapping("/test/save")
    public JsonDemoEntity save(@RequestBody JsonDemoEntity entity) {
//        JsonDemoEntity entity = new JsonDemoEntity();
//        entity.setId(6579200253588676608L);
//        entity.setName("jar");
//        entity.setJsonData(new JsonData(0L, "test"));
//        entity.setDataType(DataEnum.KEY_OK);
//        entity.setDate(new Date());
        return entity;
    }

    @PostMapping("/test/check")
    public DataEnum checkDataEnum(@RequestBody DataEnum dataEnum) {
        return dataEnum;
    }
}
