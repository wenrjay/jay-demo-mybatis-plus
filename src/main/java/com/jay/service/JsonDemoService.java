package com.jay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jay.entity.JsonDemoEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jay
 * @since 2019-11-01
 */
public interface JsonDemoService extends IService<JsonDemoEntity> {

}
