package com.jay.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jay.dao.JsonDemoMapper;
import com.jay.entity.JsonDemoEntity;
import com.jay.service.JsonDemoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jay
 * @since 2019-11-01
 */
@Service
public class JsonDemoServiceImpl extends ServiceImpl<JsonDemoMapper, JsonDemoEntity> implements JsonDemoService {

}
