package com.jay.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jay.entity.JsonDemoEntity;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jay
 * @since 2019-11-01
 */
public interface JsonDemoMapper extends BaseMapper<JsonDemoEntity> {

}
