package com.jay;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jay by 2019/08/21
 */
@SpringBootApplication(scanBasePackages = {"com.jay.*"})
@MapperScan("com.jay.dao")
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class);
    }
}
