package com.jay.entity;

import lombok.Data;

@Data
public class DefaultBaseEnum implements BaseEnum {

    private Integer value;

    @Override
    public Integer getValue() {
        return this.value;
    }
}
