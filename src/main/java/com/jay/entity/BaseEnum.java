package com.jay.entity;

import com.baomidou.mybatisplus.core.enums.IEnum;

import java.io.Serializable;

public interface BaseEnum extends IEnum {
    @Override
    Integer getValue();

}
