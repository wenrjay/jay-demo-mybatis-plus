package com.jay.entity;

import com.alibaba.fastjson.annotation.JSONType;
import com.jay.config.EnumDeserializer;

@JSONType(serializeEnumAsJavaBean = true,deserializer = EnumDeserializer.class)
public enum DataEnum implements BaseEnum {
    KEY_OK(1, "OK"),
    KEY_NO(0, "NO");

    private Integer value;
    private String text;

    DataEnum(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public String getText() {
        return text;
    }
}
