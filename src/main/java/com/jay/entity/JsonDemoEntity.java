package com.jay.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.jay.dto.JsonData;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.LongTypeHandler;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author jay
 * @since 2019-11-01
 */
@Data
@EqualsAndHashCode
@ToString
@TableName(value = "t_json_demo", autoResultMap = true)
public class JsonDemoEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private Long id;

    @TableField("name")
    private String name;

    @TableField(value = "json_data", typeHandler = FastjsonTypeHandler.class)
    private JsonData jsonData;

    @TableField("data_type")
    private DataEnum dataType;

    @TableField(value = "create_time",typeHandler = LongTypeHandler.class )
    private Date createTime;


}
