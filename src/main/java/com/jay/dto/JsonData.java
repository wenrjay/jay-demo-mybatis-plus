package com.jay.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode
@ToString
public class JsonData {

    private Long code;

    private String text;

    public JsonData() {
    }

    public JsonData(Long code, String text) {
        this.code = code;
        this.text = text;
    }
}
