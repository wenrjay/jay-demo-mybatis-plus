package com.jar.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.jay.dto.JsonData;
import com.jay.entity.DataEnum;
import com.jay.entity.JsonDemoEntity;
import com.jay.service.JsonDemoService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.interfaces.PBEKey;

@RunWith(SpringRunner.class)
@SpringBootTest()
@SpringBootApplication(scanBasePackages = "com.jay")
@ActiveProfiles("application.yml")
@Slf4j
@Configurable()
public class JsonDemoServiceTest {

    @Autowired
    private JsonDemoService jsonDemoService;

    @Test
    public void saveTest() {
        JsonDemoEntity entity = new JsonDemoEntity();
        entity.setId(6579200253588676608L);
        entity.setName("jar");
        entity.setJsonData(new JsonData(0L, "test"));
        entity.setDataType(DataEnum.KEY_OK);
        jsonDemoService.save(entity);
    }

    @Test
    public void getText() {
        JsonDemoEntity one = jsonDemoService.getOne(Wrappers.<JsonDemoEntity>query()
                .eq("json_data->'$.code'", 0L));
        log.info(one.toString());
    }

}
